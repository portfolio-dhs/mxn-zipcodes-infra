This is the infrastructure code repository for 
[mxn-zipcodes](https://gitlab.com/portfolio-dhs/mxn-zipcodes).

## Usage
Inside of every directory is a set of instructions in a README.md.

#### Build the docker containers of the app and the database before doing everything else

## Directory Structure:
- /docker:
  - Configurations to build Database and application containers and a
    docker-compose file to run them both at the same time.
- /kubernetes:
  - Definitions to deploy the app inside a load balancer.