# Instructions for Kubernetes deployment:

#### Warnings:
- You must have a Kubernetes cluster for this to run. Check GCP if you want to
  acquire one.
- You must setup your kubectl before starting
- If you have acquired the cluster by a cloud provider keep in mind that
  you will be charged for any infrastructure you deploy.

## Step 1:
First you must build the docker containers. There are already instructions
for that in the *docker* directory in the parent folder.

## Step 2:
Replace the default info with your own.
  - gitlab_secret.yaml:
    ```
    data:
      .dockerconfigjson:  PasteYourBase64EncodecDockerConfigDotJsonHere
    ```
      - Replace the  ```PasteYourBase64EncodecDockerConfigDotJsonHere``` 
        string with your own *~/.docker/config.json* **Base64 encoded** file.
  - zipcodes.yaml:
     ```
     spec:
       replicas: 3
     ```
       - Replace the ```3``` with your the amount of replicas of the app
         that will be under the load balancer.
     ```
      - name: zipcodes
          image: registry.gitlab.com/portfolio-dhs/mxn-zipcodes/master
     ```
       - Replace ```registry.gitlab.com/portfolio-dhs/mxn-zipcodes/master```
         with the name that you used the build the zipcodes/nginx
         container.
     ```
       - name: SENTRY_DSN
         value: https://put-your-dsn-here.com
     ```
       - Replace ```https://put-your-dsn-here.com``` with your own [Sentry error tracker](https://sentry.io) DSN.
     ```
     - name: DB_CONNECTION
       value: Host=dbhost;Port=5432;Database=dbname;User Id=youruser;Password=yourpasss
     ```
       - Replace ```Host=dbhost;Port=5432;Database=dbname;User Id=youruser;Password=yourpasss```
         with your own db connection string.
  - reverse-proxy.yaml:
    ```
    # Base64 encoded cert
    tls.crt: PasteYourBase64EncodedCertHere
    ```
      - Replace ```PasteYourBase64EncodedCertHere``` with your own
        SSL/TLS Base64 encoded certificate.
    ```
    # Base64 encoded key
    tls.key: PasteYourBase64EncodedKeyHere
    ```
      - Replace ```PasteYourBase64EncodedKeyHere``` with your own
        SSL/TLS Base64 encoded private key.
    ```
    rules:
      - host: mxn-zipcodes.com
    ```
      - Replace ```mxn-zipcodes.com``` with the domain name that you own.

## Step 3:
Deploy the cluster:
    
1. First you must run ```kubectl apply -f gitlab_secret.yaml```.
2. Then you must run ```kubctl apply -f zipcodes.yaml```.
3. At last you run ```kubectl apply -f reverse-proxy.yaml```.