# Instructions for building the containers
1. Database setup:
  - If you want to deploy the database manually use the zipcodes.sql to restore the database.
    Else you must continue with the steps ahead.
  - Replace the environment variables that are under database/Dockerfile
    with your desired values or you could just let default ones there.
      - POSTGRES_USER: Is the user of the database.
      - POSTGRES_PASSWORD: Is the password for that user.
      - POSTGRES_DB: Is the database name.
  - Build the database container by situating your terminal at the
    same directory of this file and use the command
    ```
    docker build -t mxn-zipcodes-db database/
    ```
2. App + Nginx setup:
  - Replace ```nginx/fullchain.pem``` content with your the ```cert-chain.pem``` of
    your domain, or if you are using let encrypt use the ```fullchain.pem``` 
    file. If you dont have any SSL/TLS certificates you can get one on
    [lets encrypt](https://letsencrypt.org/) or if you are going to use this
    app for testing you can use the default certs that are already included. 
  - Modify the ```nginx.conf``` and ``mxn-zipcodes.com``` files if you want 
    to change the distribution settings. If dont then you can just go ahead.
  - Build the API container using the command:
    ```
    docker build -t mxn-zipcodes-nginx nginx/
    ```
3. Deploy:
  - Set environment variables for the zipcodes/nginx container if you have
    changed them at all.
    - SENTRY_DSN: This is the DSN for [Sentry error tracker](https://sentry.io)
    - DB_CONNECTION: This is the database connection string, for example:
      ```
      Host=db.mxn-zipcodes.com;Port=5432;Database=zipcodes;User Id=zipcodes;Password=zipcodes
      ```
  - Now you can deploy the API with your preferred method.
    If you want to use the built-in docker-compose script use the next command:
    ```
    docker-compose up
    ```
    **Dont forget to change the environment variables that are under the docker-compose.yml**

#### There are definitions for kubernetes deployment in the kubernetes directory that is in the parent directory