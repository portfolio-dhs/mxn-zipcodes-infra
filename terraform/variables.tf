variable "AMI_ID" {
  description = "The AMI ID of the app that you have created before"
}

variable "PUBLIC_KEY" {
  description = "Public key for the SSH access .pem key"
}