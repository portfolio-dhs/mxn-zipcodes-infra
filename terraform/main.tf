provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "vm" {
  ami = var.AMI_ID
  instance_type = "t2.micro"
  security_groups = [aws_security_group.WebServer.name]
  key_name = aws_key_pair.dev_key.key_name
}

resource "aws_key_pair" "dev_key" {
  public_key = var.PUBLIC_KEY
}
resource "aws_security_group" "WebServer" {
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmpv6"
    ipv6_cidr_blocks = ["::/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

//noinspection HILUnresolvedReference
output "instance_ip" {
  value = [aws_instance.vm.*.public_ip]
}